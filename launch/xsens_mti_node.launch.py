from launch import LaunchDescription
from launch.actions import SetEnvironmentVariable, DeclareLaunchArgument
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
from pathlib import Path
from launch.substitutions import LaunchConfiguration


def generate_launch_description():

    # we can use two topics, based on the device_id
    # if we first find out which imu is which we can pass more comprehensible names
    # we don't want to pass two parameter files, so we would like for the node to publish to the device_id
    # maybe in the head launcher, we could try to get the ids (converted to hex) of both IMU's
    # then pass those device_id's
    ld = LaunchDescription()


    # Set env var to print messages to stdout immediately
    arg = SetEnvironmentVariable('RCUTILS_CONSOLE_STDOUT_LINE_BUFFERED', '1')
    # arg2 = SetEnvironmentVariable('RCUTILS_LOGGING_BUFFERED_STREAM', '1')
    ld.add_action(arg)
    # ld.add_action(arg2)

    parameters_file_path = LaunchConfiguration('node_config', default=Path(get_package_share_directory('bluespace_ai_xsens_mti_driver'), 'param', 'xsens_mti_node.yaml'))
    node_name = LaunchConfiguration('node_name', default='xsens_mti_node')

    # parameters_file_path = Path(get_package_share_directory('bluespace_ai_xsens_mti_driver'), 'param', 'xsens_mti_node.yaml')
    xsens_mti_node = Node(
        package='bluespace_ai_xsens_mti_driver',
        executable='xsens_mti_node',
        name=node_name,
        output='screen',
        parameters=[parameters_file_path],
        arguments=[]
        # arguments=['--ros-args', '--log-level', 'debug']
    )
    ld.add_action(xsens_mti_node)

    return ld
