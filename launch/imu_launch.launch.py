from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import IncludeLaunchDescription
from ament_index_python.packages import get_package_share_directory
from pathlib import Path
from launch_ros.substitutions import FindPackageShare
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.actions import DeclareLaunchArgument
from launch.substitutions import PathJoinSubstitution, TextSubstitution

# from xsens_mti_node.launch import generate_launch_description

def generate_launch_description():

    upper_parameters_file_path = Path(get_package_share_directory('bluespace_ai_xsens_mti_driver'), 'param', "xsens_mti_node_upper.yaml")
    lower_parameters_file_path = Path(get_package_share_directory('bluespace_ai_xsens_mti_driver'), 'param', "xsens_mti_node_lower.yaml")
    upper_xsens_mti_node = "upper_imu"
    lower_xsens_mti_node = "lower_imu"


    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('bluespace_ai_xsens_mti_driver'), 'launch', 'xsens_mti_node.launch.py'
                ]),
            ]),
            launch_arguments={
                'node_name': upper_xsens_mti_node,
                'node_config': str(upper_parameters_file_path)
            }.items()
        ),
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('bluespace_ai_xsens_mti_driver'), 'launch', 'xsens_mti_node.launch.py'
                ]),
            ]),
            launch_arguments={
                'node_name' : lower_xsens_mti_node,
                'node_config': str(lower_parameters_file_path)
            }.items()
        )
    ])
